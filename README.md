# Kohana
Demo Kohana with Jquery
## Installation
 Run the folder on your server, setup virtualhost with your apache, if you are on windows, go to C:Windows/system32/drivers/etc/hosts and edit it, add a local address that you setup on virtual host
## Usage
 1. import sql -> folder is on "\imports\kohanatest.sql"
 2. edit application/config/database.php for your database details, please change the port I am using 3308 on sample
## History
 September 10, Initial push
## Credits
 Bootstrap, Kohana
## License
 None, Free