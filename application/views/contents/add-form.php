<form class="form-horizontal"  id="addSubmit">
<fieldset>

<!-- Form Name -->
<legend>Add Data</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Title</label>  
  <div class="col-md-5">
  <input id="textinput" name="textinput" placeholder="placeholder" class="form-control input-md" required="" type="text">
  <span class="help-block">Input title text</span>  
  </div>
</div>

<!-- File Button --> 
<div class="form-group">
  <label class="col-md-4 control-label" for="filebutton">Image</label>
  <div class="col-md-4">
    <input id="filebutton" name="filebutton" class="input-file" type="file">
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="singlebutton"></label>
  <div class="col-md-4">
    <button id="save" type="submit" name="singlebutton" class="btn btn-primary">Save</button>
  </div>
</div>

</fieldset>
</form>
