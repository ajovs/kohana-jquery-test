<!-- Modal -->
<div id="modalEdit" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">-- + --</h4>
      </div>
      <div class="modal-body">
        
        <form class="form-horizontal" id="updateSubmit" >
        
       <fieldset>

		<!-- Form Name -->
		<legend>Update Data</legend>

		<!-- Text input-->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="textinput">Title</label>  
		  <div class="col-md-5">
		  <input id="textinput" name="edittext" placeholder="Your Title here" class="form-control input-md edittitle" required="" type="text">
		  <input type="hidden" class="editid" name = "edittextid" />
		  <span class="help-block">Input title text</span>  
		  </div>
		</div>
		
		<!-- image value //-->
		<div class="form-group">
	   <label class="col-md-4 control-label" for="textinput">Current Image:</label>  
		   <div class="col-md-4">
		      <img  class="image-preview" width="auto" /> 
		      <span class="help-block">Click Browse to replace image</span>  
			</div>
		</div>

		<!-- File Button --> 
		<div class="form-group">
		  <label class="col-md-4 control-label" for="filebutton">Image</label>
		  <div class="col-md-4">
			<input id="filebutton" name="editfile" class="input-file editimage" type="file">
			<input type="hidden" class="editoldimage" name = "editoldimage" />
		  </div>
		</div>

		<!-- Button -->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="singlebutton"></label>
		  <div class="col-md-4">
			<button id="updatebutton" name="singlebutton" class="btn btn-primary update-data">Update</button>
		  </div>
		</div>

		</fieldset>
	 
        </form>

        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>