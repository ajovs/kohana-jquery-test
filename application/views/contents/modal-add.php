<!-- Modal -->
<div id="modalAdd" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">-- + --</h4>
      </div>
      <div class="modal-body">
        
        <form class="form-horizontal" id="addSubmit" >
        
       <fieldset>

		<!-- Form Name -->
		<legend>Add Data</legend>

		<!-- Text input-->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="textinput">Title</label>  
		  <div class="col-md-5">
		  <input id="textinput" name="titletext" placeholder="Your Title here" class="form-control input-md" required="" type="text">
		  <span class="help-block">Input title text</span>  
		  </div>
		</div>

		<!-- File Button --> 
		<div class="form-group">
		  <label class="col-md-4 control-label" for="filebutton">Image</label>
		  <div class="col-md-4">
			<input id="filebutton" name="filebutton" class="input-file" type="file">
		  </div>
		</div>

		<!-- Button -->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="singlebutton"></label>
		  <div class="col-md-4">
			<button id="savebutton" name="singlebutton" class="btn btn-primary save-data">Save</button>
		  </div>
		</div>

		</fieldset>
	 
        </form>

        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>