
    <div class="row">
        <div class="col-md-12 add-padding-vertical">
         <button type="button" class="btn btn-info btn-lg pull-right" data-toggle="modal" data-target="#modalAdd">Add</button>  
		</div>
    </div> <!-- add button //-->
	<div class="row">
        <div class="col-md-3">
            <form action="#" method="get">
                <div class="input-group">
                    
                    <input class="form-control" id="system-search" name="q" placeholder="Search for" required>
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                    </span>
                </div>
            </form>
        </div>
		<div class="col-md-9">
    	 <table class="table table-list-search data-list-upload">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Image</th>
                            <th>Title</th>
                            <th>Date Added</th>
                            <th>Date Updated</th>
                            <th>Functions</th>
                        </tr>
                    </thead>
                    <tbody class="tbody-content">
                       
                    </tbody>
                </table>   
		</div>
	</div>
