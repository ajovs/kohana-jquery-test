<?php defined('SYSPATH') or die('No direct script access.');

class Controller_RestApi extends Controller {

	//Show all current uploads, data
	public function action_index()
	{
		
		
		$model = Model::factory('Tester');
		$query = $model->loadData();
		
		
		$json = json_encode($query);
		echo $json;
		
	}
	
	//Add data, Method Post
	public function action_add()
	{
		/*
		 * Fields
		 *  id
		 *	title
		 *  thumbnail
		 *	filename
		 *	created
		 *	updated
		 */
		$model = Model::factory('Tester');

		
		//Upload image first
		$the_file = $model->upload_image($_FILES['filebutton']);

		
		$table = "tester";
		$array_fields = array('title', 'thumbnail', 'filename', 'created');
		$array_values = array($_POST['titletext'], 'thumbnail_'.$the_file, $the_file, DB::expr('Now()'));
		
		$query = $model->save_upload($table, $array_fields, $array_values);
		
		$last_insert = $query[0];
		
		$query_output = $model->singleData($last_insert);

		
		$json = json_encode($query_output);
		echo $json;
		
		
	}
	
	//edit view
	public function action_edit()
	{
		$model = Model::factory('Tester');
		
		//pre populate
		$query_output = $model->singleData($_POST['id']);

		
		$json = json_encode($query_output);
		echo $json;
		
	}
	
	//update data, Method Post
	public function action_update()
	{
		$model = Model::factory('Tester');
		
		//Upload image first IF $_FILES is not empty
		if(isset($_FILES['editfile'])){
			$the_file = $model->upload_image($_FILES['editfile']);
		} else {
			$the_file = $_POST['editoldimage'];
		}
		

		
		$table = "tester";
		$array_fields_values = array('title' => $_POST['edittext'], 
									 'thumbnail' => 'thumbnail_'.$the_file, 
									 'filename' => $the_file);
		
		$query_update = $model->update_data($table, $array_fields_values,  $_POST['edittextid']);
		
		//Just reload
		$query = $model->loadData();
		
		$json = json_encode($query);
		echo $json;	
	}
	
    //Process delete function
	public function action_delete()
	{
		$model = Model::factory('Tester');
		
		$query = $model->delete_data($_POST['id']);

		$json = json_encode($_POST['id']);
		echo $json;
	}
} 
