<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Front extends Controller {

	//Front Site 
	public function action_index()
	{
		 $view = View::factory('front/index');
 
         $view->title = "Kohana Test";
         $view->body = View::factory('contents/list');
		 $view->modal_add = View::factory('contents/modal-add');
		 $view->modal_edit = View::factory('contents/modal-edit');
		
		$this->response->body($view);
		
	}

} 
