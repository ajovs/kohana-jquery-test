<?php
/**
 * Description of tester
 *
 * @author Aaron Jefferson Villanueva
 */
class Model_Tester extends Model{

	/*
     * Load the news for the first page
     */
  public function loadData(){
	  
	  
    return DB::select()->from('tester')
		               ->order_by('id', 'DESC')
		               ->execute()
		               ->as_array()
		               ;
  }
	
   public function singleData($id){
	  
	  
    return DB::select()->from('tester')
		               ->where('id', '=', $id)
		               ->order_by('id', 'DESC')
		               ->execute()
		               ->as_array()
		               ;
  }
	
  //Save data	
  public function save_upload($table, $array_fields, $array_values)
  {
	 return DB::insert($table, $array_fields)->values($array_values)
											 ->execute()
											 ;   
  }
	
  //Delete Data
  public function delete_data($id)
  {
    return $query = DB::delete('tester')->where('id', 'IN', array($id))
		                                ->execute()
		                                ;
  }
	
  //Update Data
  public function update_data($table, $array_fields_values,  $id)
  {
	  return DB::update($table)->set($array_fields_values)
		                          ->where('id', '=', $id)
		                          ->execute()
		                          ;
  }
	
  //Image upload
  public function upload_image($filedata) 
  {
	    $error_message = NULL;
        $filename = NULL;
 
 
                $filename = $this->_save_image($filedata);
        
 
        if ( ! $filename)
        {
            $error_message = 'There was a problem while uploading the image.
                Make sure it is uploaded and must be JPG/PNG/GIF file.';
        }
 
        
	 //return filename
	  return $filename;
  }
	
   protected function _save_image($image)
   {
        if (
            ! Upload::valid($image) OR
            ! Upload::not_empty($image) OR
            ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
        {
            return FALSE;
        }
 
        $directory = DOCROOT.'uploads/images/';
 
        if ($file = Upload::save($image, NULL, $directory))
        {
            $filename = strtolower(Text::random('alnum', 20)).'.jpg';
             
			//Thumbnail
            Image::factory($file)
                ->resize(200, 200, Image::AUTO)
                ->save($directory.'thumbnail_'.$filename);
			
			//original
            Image::factory($file)
                ->save($directory.$filename);
 
            // Delete the temporary file
            unlink($file);
 
            return $filename;
        }
 
        return FALSE;
    }	
}