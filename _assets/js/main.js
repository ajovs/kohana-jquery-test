/* 
 * @author: Aaron Jefferson Villanueva
 * @description: Kohana Framework + Jquery testing
 *
 */

$("#addSubmit").on('submit',(function(e) {
    e.preventDefault();
    $.ajax({
        url: "/restapi/add",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
		dataType:"json", //to parse string into JSON object,
        success: function(data){ 
			alert("Added Data");
			$('#modalAdd').modal('hide');
             if(data){
				 console.log(data);
                var len = data.length;
                var txt = "";
                if(len > 0){
                    for(var i=0;i<len;i++){
						
                        if(data[i].id && data[i].title){
                            txt += "<tr><td>"+data[i].id+"</td><td><a href = '/uploads/images/"+data[i].filename+"' target = '_blank'><img src = '/uploads/images/"+data[i].thumbnail+"' class='img-circle' width = 'auto' height = '100' /></a> </td><td>"+data[i].title+"</td><td>"+data[i].created+"</td><td>"+data[i].updated+"</td><td> <a href='#' rel-aj-edit-id = '"+data[i].id+"' class='btn btn-primary a-btn-edit-but'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span></a> <a href='#' rel-aj-del-id = '"+data[i].id+"' class='btn btn-danger a-btn-del-but'><span class='glyphicon glyphicon-remove'  aria-hidden='true'></span></a> </td></tr>";
                        }
                    }
                    if(txt != ""){
                        $(".tbody-content").append(txt);
                    }
                }
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            alert('error: ' + textStatus + ': ' + errorThrown);
        }          
    });
}));


//Initial list
$( document ).ready(function() {
     $.ajax({
        type: 'POST',
        url: '/restapi',
        data: $('.data-list-upload').serialize(), //future search function as AJAX
        dataType:"json", //to parse string into JSON object,
        success: function(data){ 
            if(data){
                var len = data.length;
                var txt = "";
                if(len > 0){
                    for(var i=0;i<len;i++){
                        if(data[i].id && data[i].title){
                            txt += "<tr><td>"+data[i].id+"</td><td><a href = '/uploads/images/"+data[i].filename+"' target = '_blank'><img src = '/uploads/images/"+data[i].thumbnail+"' class='img-circle' width = 'auto' height = '100' /></a> </td><td>"+data[i].title+"</td><td>"+data[i].created+"</td><td>"+data[i].updated+"</td><td> <a href='#' rel-aj-edit-id = '"+data[i].id+"' class='btn btn-primary a-btn-edit'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span></a> <a href='#' rel-aj-del-id = '"+data[i].id+"' class='btn btn-danger a-btn-del-but'><span class='glyphicon glyphicon-remove'  aria-hidden='true'></span></a> </td></tr>";
                        }
                    }
                    if(txt != ""){
                        $(".tbody-content").append(txt).removeClass("hidden");
                    }
                }
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            alert('error: ' + textStatus + ': ' + errorThrown);
        }
    }); 
	
});

$("#updateSubmit").on('submit',(function(e) {
    e.preventDefault();
    $.ajax({
        url: "/restapi/update",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
		dataType:"json", //to parse string into JSON object,
        success: function(data){ 
			alert("Updated Data");
			$('#modalEdit').modal('hide');
             if(data){
                var len = data.length;
                var txt = "";
                 if(len > 0){
                    for(var i=0;i<len;i++){
                        if(data[i].id && data[i].title){
                            txt += "<tr><td>"+data[i].id+"</td><td><a href = '/uploads/images/"+data[i].filename+"' target = '_blank'><img src = '/uploads/images/"+data[i].thumbnail+"' class='img-circle' width = 'auto' height = '100' /></a> </td><td>"+data[i].title+"</td><td>"+data[i].created+"</td><td>"+data[i].updated+"</td><td> <a href='#' rel-aj-edit-id = '"+data[i].id+"' class='btn btn-primary a-btn-edit'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span></a> <a href='#' rel-aj-del-id = '"+data[i].id+"' class='btn btn-danger a-btn-del-but'><span class='glyphicon glyphicon-remove'  aria-hidden='true'></span></a> </td></tr>";
                        }
                    }
                    if(txt != ""){
						$(".tbody-content").empty();
                        $(".tbody-content").append(txt);
                    }
                }
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            alert('error: ' + textStatus + ': ' + errorThrown);
        }          
    });
}));

$('body').on('click', '.a-btn-del-but', function(){
	var id_num = $(this).attr('rel-aj-del-id');
	var that = $(this);
	
	$.ajax({
		type: 'POST',
		url: '/restapi/delete',
		data: { 
			'id': id_num
		},
		success: function(data){
			
		   alert("ID: " + data + " Deleted");	
		   that.closest("tr").remove();
		},
		error: function(jqXHR, textStatus, errorThrown){
				alert('error: ' + textStatus + ': ' + errorThrown);
		}	
    });
	
	
    //alert($(this).attr('rel-aj-del-id'));
});

//Get edit data
$('body').on('click', '.a-btn-edit', function(){
	 var id_num = $(this).attr('rel-aj-edit-id');
	 var that = $(this);
	
	 $.ajax({
        type: 'POST',
        url: '/restapi/edit',
        data: { 
			'id': id_num
		},
        dataType:"json", //to parse string into JSON object,
        success: function(data){ 
            if(data){
              $('.editid').val(data[0].id);
			  $('.edittitle').val(data[0].title);
			 // $('.editimage').val(data[0].filename);	
			  $('.editoldimage').val(data[0].filename);
			  //Load image
			  $('.image-preview').attr('src','/uploads/images/thumbnail_'+data[0].filename);	
				
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            alert('error: ' + textStatus + ': ' + errorThrown);
        }
    }); 
   $('#modalEdit').modal('show');
});
	
